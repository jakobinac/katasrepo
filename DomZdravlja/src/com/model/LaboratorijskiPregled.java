package com.model;

public class LaboratorijskiPregled {
	
	
	String datumPregleda;
	String vremePregleda;
	
	TipLaboratorijskogPregleda tipLaboratorijskogPregleda;

	public LaboratorijskiPregled() {
		super();
	}


	public LaboratorijskiPregled(String datumPregleda, String vremePregleda) {
		super();
		this.datumPregleda = datumPregleda;
		this.vremePregleda = vremePregleda;
	}


	public String getDatumPregleda() {
		return datumPregleda;
	}


	public void setDatumPregleda(String datumPregleda) {
		this.datumPregleda = datumPregleda;
	}


	public String getVremePregleda() {
		return vremePregleda;
	}


	public void setVremePregleda(String vremePregleda) {
		this.vremePregleda = vremePregleda;
	}


	public TipLaboratorijskogPregleda getTipLaboratorijskogPregleda() {
		return tipLaboratorijskogPregleda;
	}


	public void setTipLaboratorijskogPregleda(TipLaboratorijskogPregleda tipLaboratorijskogPregleda) {
		this.tipLaboratorijskogPregleda = tipLaboratorijskogPregleda;
	}


	

	
	
	
	
	
	
	
	
	
	
	
	

}
