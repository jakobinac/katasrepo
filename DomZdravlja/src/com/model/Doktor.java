package com.model;

import java.util.ArrayList;


public class Doktor extends Osoba {
	
	Long id;
	
	String specijalnost;
	
	ArrayList<Pacijent> pacijenti = new ArrayList<>();
	
	public Doktor() {
		super();
	}

	public Doktor(Long id, String specijalnost, ArrayList<Pacijent> pacijenti) {
		super();
		this.id = id;
		this.specijalnost = specijalnost;
		this.pacijenti = pacijenti;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpecijalnost() {
		return specijalnost;
	}

	@Override
	public void setSpecijalnost(String specijalnost) {
		this.specijalnost = specijalnost;
	}

	public ArrayList<Pacijent> getPacijenti() {
		return pacijenti;
	}

	@Override
	public void setPacijenti(ArrayList<Pacijent> pacijenti) {
		this.pacijenti = pacijenti;
	}


	
}
