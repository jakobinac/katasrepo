package com.model;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

public class Pacijent extends Osoba {
	
	Long id;
	Integer jmbg;
	Integer brKartona;
	
	Doktor doktor;

	public Pacijent() {
		super();
	}
	
	

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Pacijent(Long id, Integer jmbg, Integer brKartona, Doktor doktor) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.brKartona = brKartona;
		this.doktor = doktor;
	}



	public Pacijent(Integer jmbg, Integer brKartona, Doktor doktor) {
		super();
		this.jmbg = jmbg;
		this.brKartona = brKartona;
		this.doktor = doktor;
	}

	public Integer getJmbg() {
		return jmbg;
	}

	public void setJmbg(Integer jmbg) {
		this.jmbg = jmbg;
	}

	public Integer getBrKartona() {
		return brKartona;
	}

	public void setBrKartona(Integer brKartona) {
		this.brKartona = brKartona;
	}

	public Doktor getDoktor() {
		return doktor;
	}

	public void setDoktor(Doktor doktor) {
		this.doktor = doktor;
	}

	
	
	
	

}
