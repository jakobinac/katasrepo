package com.model;

public class KrvniPritisak extends TipLaboratorijskogPregleda{
	
	private double gornjaVrednost;
	
	private double donjaVrednost;
	
	private int puls;
	
	public KrvniPritisak() {
			
	}

	public KrvniPritisak(double gornjaVrednost, double donjaVrednost, int puls) {
		
		this.gornjaVrednost = gornjaVrednost;
		this.donjaVrednost = donjaVrednost;
		this.puls = puls;
	}

	public double getGornjaVrednost() {
		return gornjaVrednost;
	}

	@Override
	public void setGornjaVrednost(double gornjaVrednost) {
		this.gornjaVrednost = gornjaVrednost;
	}

	public double getDonjaVrednost() {
		return donjaVrednost;
	}

	@Override
	public void setDonjaVrednost(double donjaVrednost) {
		this.donjaVrednost = donjaVrednost;
	}

	public int getPuls() {
		return puls;
	}

	@Override
	public void setPuls(int puls) {
		this.puls = puls;
	}
	
	
	
	

}
