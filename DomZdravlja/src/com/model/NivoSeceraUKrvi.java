package com.model;

public class NivoSeceraUKrvi extends TipLaboratorijskogPregleda{
	
	double vrednost;
	
	String vremePoslednjegObroka;

	public NivoSeceraUKrvi(double vrednost, String vremePoslednjegObroka) {
		super();
		this.vrednost = vrednost;
		this.vremePoslednjegObroka = vremePoslednjegObroka;
	}

	public NivoSeceraUKrvi() {
		super();
	}

	public double getVrednost() {
		return vrednost;
	}

	public void setVrednost(double vrednost) {
		this.vrednost = vrednost;
	}

	public String getVremePoslednjegObroka() {
		return vremePoslednjegObroka;
	}

	public void setVremePoslednjegObroka(String vremePoslednjegObroka) {
		this.vremePoslednjegObroka = vremePoslednjegObroka;
	}
	
	
	
	

}
