package com.model;

import java.util.ArrayList;
import java.util.Scanner;

public class Test {
	
	public static ArrayList<Doktor> doktori = new ArrayList<>();
	
	public static ArrayList<Pacijent> pacijenti = new ArrayList<>();

	public static Scanner tastatura = new Scanner(System.in);

	public static void main(String[] args) {
		 Doktor d1 = new Doktor();
		 Pacijent p1 = new Pacijent();
		LaboratorijskiPregled lp1 = new LaboratorijskiPregled();

		String answer = null;

		do {

			System.out.println("Meni:");
			System.out.println("1. Kreiraj doktora");
			System.out.println("2. Kreiraj pacijenta ");
			System.out.println("3. Pacijent “Dragan” bira doktora “Milan” za svog izabranog lekara ");
			System.out.println(
					"4. Doktor “Milan” zakazuje pregled za merenje nivoa šećera u krvi za pacijenta “Dragan” ");
			System.out.println("5. Doktor “Milan” zakazuje pregled za merenje krvnog pritiska za pacijenta “Dragan” ");
			System.out.println(
					"6. Pacijent “Dragan” obavlja laboratorijski pregled za merenje nivoa šećera u krvi. Simulirati i prikazati rezultate. ");
			System.out.println(
					"7. Pacijent “Dragan” obavlja laboratorijski pregled za merenje krvnog pritiska. Simulirati i\r\n"
							+ "prikazati rezultate. ");
			System.out.println("x. Izlaz");

			answer = tastatura.nextLine();

			switch (answer) {
			case "1":
				kreirajDoktora(d1);

				break;
			case "2":
				kreirajPacijenta();

				break;
			case "3":
				ispisi();

				break;
			case "4":
				izaberiPregled();

				break;
			case "5":

				break;
			case "6":
				nalazSecer();

				break;
			case "7":

				break;

			case "x":
				break;
			default:
				System.out.println("Pogresan izbor opcije. Pokusajte ponovo.");
			}

		} while (!answer.equals("x"));

	}

	private static void nalazSecer() {
		
//		TipLaboratorijskogPregleda tip1 = new KrvniPritisak(gornjaVrednost, donjaVrednost, puls)
		TipLaboratorijskogPregleda tip2 = new NivoHolesterolaUKrvi();
		
		
		System.out.println("Unesite rezultate merenja secera");
		tastatura.nextLine();
		
		
		
	}

	private static void izaberiPregled() {

		String imeDoktora;
		String imePacijenta;
		String tipPregleda = null;

		do {
			System.out.println("Unesite ime doktora ( milan ) ");
			imeDoktora = tastatura.nextLine();
			imeDoktora = imeDoktora.substring(0, 1).toUpperCase() + imeDoktora.substring(1).toLowerCase();
		} while (!imeDoktoraPostoji(imeDoktora));

		do {
			System.out.println("Unesite ime pacijenta ( dragan )");
			imePacijenta = tastatura.nextLine();
			imePacijenta = imePacijenta.substring(0, 1).toUpperCase() + imePacijenta.substring(1).toLowerCase();
		} while (!imePacijentaPostoji(imePacijenta));

		do {
			System.out.println("Izaberite tip pregleda");
			System.out.println("1. Merenje krvnog pritiska");
			System.out.println("2. Merenje nivoa secera u krvi");
			System.out.println("3. Merenje nivoa holesterola u krvi");
			System.out.println("x. Izadji");

			tipPregleda = tastatura.nextLine();

			switch (tipPregleda) {
			case "1":
				System.out.println("Doktor " + imeDoktora + " zakazuje pregled za merenje krvnog pritiska za pacijenta "
						+ imePacijenta);
				break;
			case "2":
				System.out.println("Doktor " + imeDoktora
						+ " zakazuje pregled za merenje nivoa secera u krvi za pacijenta " + imePacijenta);
				break;
			case "3":
				System.out.println("Doktor " + imeDoktora
						+ " zakazuje pregled za merenje nivoa holesterola za pacijenta " + imePacijenta);
				break;

			}

		} while (!tipPregleda.equals("x"));

	}

	private static void ispisi() {

//		System.out.println("Pacijent " + p1.getIme() + " bira doktora " + d1.getIme() + " za svog izabranog lekara.");

	}

	private static void kreirajPacijenta() {
		Pacijent p1 = new Pacijent();
		p1.setIme("Dragan");
		p1.setPrezime("Petrovic");
		p1.setJmbg(748271842);
		p1.setBrKartona(41421421);

		pacijenti.add(p1);

		System.out.println("Kreiran pacijent : " + p1.getIme() + " " + p1.getPrezime() + " " + p1.getJmbg() + " "
				+ p1.getBrKartona());

	}

	private static void kreirajDoktora(Doktor d1) {
		
		
		d1.setIme("Milan");
		d1.setPrezime("Milanovic");
		d1.setSpecijalnost("Lekar opste prakse");
		doktori.add(d1);
		System.out.println("Kreiran doktor : " + d1.getIme() + " " + d1.getPrezime() + " " + d1.getSpecijalnost());

	}

	public static boolean imeDoktoraPostoji(String imeDoktora) {
		for(int i = 0; i < doktori.size(); i++) {
			if(imeDoktora.equalsIgnoreCase(doktori.get(i).getIme())) {
				return true;
			}
		}
		return false;
	}


	public static boolean imePacijentaPostoji(String imePacijenta) {
		for (int i = 0 ; i < pacijenti.size(); i++) {
			if(imePacijenta.equalsIgnoreCase(pacijenti.get(i).getIme())) {
				
				return true;
			}
		}
		return false;
	}

}
