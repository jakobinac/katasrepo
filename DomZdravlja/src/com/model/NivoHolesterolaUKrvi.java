package com.model;

public class NivoHolesterolaUKrvi extends TipLaboratorijskogPregleda{
	
	private double vrednost;
	
	private String vremePoslednjegObroka;
	
	

	public NivoHolesterolaUKrvi() {
		super();
	}

	public NivoHolesterolaUKrvi(double vrednost, String vremePoslednjegObroka) {
		super();
		this.vrednost = vrednost;
		this.vremePoslednjegObroka = vremePoslednjegObroka;
	}

	@Override
	public void setVrednost(double vrednost) {
		this.vrednost = vrednost;
	}

	@Override
	public void setVremePoslednjegObroka(String vremePoslednjegObroka) {
		this.vremePoslednjegObroka = vremePoslednjegObroka;
	}

	public double getVrednost() {
		return vrednost;
	}

	public String getVremePoslednjegObroka() {
		return vremePoslednjegObroka;
	}
	
	

}
