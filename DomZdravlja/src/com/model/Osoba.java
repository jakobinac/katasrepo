package com.model;

import java.util.ArrayList;

import org.apache.log4j.Logger;

public abstract class Osoba {
	
	
	String ime;
	String prezime;
	
	public Osoba() {
		super();
	}


	public Osoba(String ime, String prezime) {
		super();
		this.ime = ime;
		this.prezime = prezime;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	public void setSpecijalnost(String specijalnost) {
		
	}
	
	public void setPacijenti(ArrayList<Pacijent> pacijenti) {
		
	}
	


}
