var knjizaraApp = angular.module("knjizaraApp", ['ngRoute']);

knjizaraApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/',{
        templateUrl: '/app/html/partial/piva.html'
    }).when('/piva/edit/:id',{
        templateUrl: '/app/html/partial/edit-piva.html'
    }).when('/piva',{
        templateUrl: '/app/html/partial/piva.html'
    }).when('/pivare',{
        templateUrl: '/app/html/partial/pivare.html'
    }).otherwise({
        redirectTo: '/'
    });
}]);

knjizaraApp.controller("pivaCtrl", function($scope, $http, $location){
	console.log("Ulazak u ctrl");
	var baseUrlPivare = "/api/pivare";
    var baseUrlPiva = "/api/piva";

    $scope.rezimDodavanja = true;

    $scope.promeniRezim = function(){
        $scope.rezimDodavanja = !$scope.rezimDodavanja;
    };

    $scope.pageNum = 0;
    $scope.totalPages = 1;

    $scope.pivare = [];
    $scope.piva = [];

    $scope.novoPivo = {};
    $scope.trazenoPivo = {};



    var getPiva = function(){

    	 var config = {params: {}};

    	 config.params.pageNum = $scope.pageNum;

         if($scope.trazenoPivo.naziv != ""){
             config.params.naziv = $scope.trazenoPivo.naziv;
         }

         if($scope.trazenoPivo.minIbu != ""){
             config.params.minIbu = $scope.trazenoPivo.minIbu;
         }

         if($scope.trazenoPivo.maxIbu != ""){
             config.params.maxIbu = $scope.trazenoPivo.maxIbu;
         }

         if($scope.trazenoPivo.pivaraId != ""){
             config.params.pivaraId = $scope.trazenoPivo.pivaraId;
         }


         $http.get(baseUrlPiva, config)
         .then(function success(data){
             $scope.piva = data.data;
             $scope.totalPages = data.headers('totalPages');

         },
         		function error(data){
        	 alert("Nije uspesno dobavljanje piva")
         });

       }

         var getPivare = function(){

             $http.get(baseUrlPivare)
                 .then(function success(data){
                     $scope.pivare = data.data;
                 },
                 function error(data){
                	 alert("Neuspesno pribavljanje podataka o pivarama")
                 }

                 );

         };



         getPiva();
         getPivare();

         $scope.nazad = function(){
             if($scope.pageNum > 0) {
                 $scope.pageNum = $scope.pageNum - 1;
                 getPiva();
             }
         };

         $scope.napred = function(){
             if($scope.pageNum < $scope.totalPages - 1){
                 $scope.pageNum = $scope.pageNum + 1;
                 getPiva();
             }
         };


         $scope.dodaj = function(){
           console.log("$scope.novoPivo) %o",$scope.novoPivo);
             $http.post(baseUrlPiva, $scope.novoPivo).then(function success(data){
                 	getPiva();


                 },
                   	   function error(data){
                	 alert("Nije uspesno ubaceno novo pivo");
                 });
         };


         $scope.trazi = function () {
             $scope.pageNum = 0;
             getPiva();
         }

         $scope.izmeni = function(id){
             $location.path('/piva/edit/' + id);
         }

         $scope.obrisi = function(id){
             $http.delete(baseUrlPiva + "/" + id).then(
                 function success(data){
                 	getPiva();
                 },
                 function error(data){
                     alert("Neuspesno brisanje!");
                 }
             );
         }






    })




knjizaraApp.controller("editPivaCtrl", function($scope, $http, $routeParams, $location){

    var baseUrlPiva = "/api/piva";

    $scope.staroPivo = null;

    var getStaroPivo = function(){

        $http.get(baseUrlPiva + "/" + $routeParams.id)
            .then(
            	function success(data){
            		$scope.staroPivo = data.data;
            	},
            	function error(data){
            		alert("Neušpesno dobavljanje piva.");
            	}
            );

    }
    getStaroPivo();

    $scope.izmeni = function(){
        $http.put(baseUrlPiva + "/" + $scope.staroPivo.id, $scope.staroPivo)
            .then(
        		function success(data){
        			alert("Uspešno izmenjen objekat!");
        			$location.path("/");
        		},
        		function error(data){
        			alert("Neuspešna izmena piva.");
        		}
            );
    }
});


knjizaraApp.controller("pivaraCtrl", function($scope, $http, $routeParams, $location){

    var baseUrlPivare = "/api/pivare";

    $scope.pivare = [];

    $scope.novaPivara = {};
    $scope.novaPivara.naziv = "";
    $scope.novaPivara.pib = "";
    $scope.novaPivara.drzava = "";

	$scope.pageNum = 0;
	$scope.totalPages = 1;

});
