package jwd.knjizara.service;

import org.springframework.data.domain.Page;

import jwd.knjizara.model.Pivo;

public interface PivoService {
	
	Page<Pivo> findAll(int pageNum);
	Pivo findOne(Long id);
	void save(Pivo pivo);
	void remove(Long id);
	
	
	Page<Pivo> pretraga(String naziv, Double minIbu, Double maxIbu, Long pivaraId, int pageNum);
	Page<Pivo> findByPivaraId(int pageNum, Long pivaraId);

}
