package jwd.knjizara.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.knjizara.model.Pivara;
import jwd.knjizara.repository.PivaraRepository;
import jwd.knjizara.service.PivaraService;

@Service
@Transactional
public class JpaPivaraServiceImpl implements PivaraService {
	
	@Autowired
	private PivaraRepository pivaraRepository;

	@Override
	public Page<Pivara> findAll(int pageNum) {
		return pivaraRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Pivara findOne(Long id) {
		// TODO Auto-generated method stub
		return pivaraRepository.findOne(id);
	}

	@Override
	public void save(Pivara pivara) {
		pivaraRepository.save(pivara);
		
	}

	@Override
	public void remove(Long id) {
		pivaraRepository.delete(id);
		
	}

}
