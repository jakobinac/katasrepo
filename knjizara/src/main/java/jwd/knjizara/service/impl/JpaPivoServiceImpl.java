package jwd.knjizara.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.knjizara.model.Pivo;
import jwd.knjizara.repository.PivoRepository;
import jwd.knjizara.service.PivoService;

@Service
@Transactional
public class JpaPivoServiceImpl implements PivoService {
	
	@Autowired
	private PivoRepository pivoRepository;

	@Override
	public Page<Pivo> findAll(int pageNum) {
		
		return pivoRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Pivo findOne(Long id) {
		
		return pivoRepository.findOne(id);
	}

	@Override
	public void save(Pivo pivo) {
		pivoRepository.save(pivo);
		
	}

	@Override
	public void remove(Long id) {
		pivoRepository.delete(id);
		
	}

	@Override
	public Page<Pivo> pretraga(String naziv, Double minIbu, Double maxIbu, Long pivaraId, int pageNum) {
		// TODO Auto-generated method stub
		return pivoRepository.pretraga(naziv, minIbu, maxIbu, pivaraId, new PageRequest(pageNum, 5));
	}

	@Override
	public Page<Pivo> findByPivaraId(int pageNum, Long pivaraId) {
		// TODO Auto-generated method stub
		return pivoRepository.findByPivaraId(pivaraId, new PageRequest(pageNum, 5));
	}

}
