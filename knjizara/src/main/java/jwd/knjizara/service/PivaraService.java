package jwd.knjizara.service;

import org.springframework.data.domain.Page;

import jwd.knjizara.model.Pivara;

public interface PivaraService {
	
	Page<Pivara> findAll(int pageNum);
	Pivara findOne(Long id);
	void save(Pivara pivara);
	void remove(Long id);

}
