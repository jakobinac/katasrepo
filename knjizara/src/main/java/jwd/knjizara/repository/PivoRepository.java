package jwd.knjizara.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.knjizara.model.Pivo;

@Repository
public interface PivoRepository extends JpaRepository<Pivo, Long>{
	
	Page<Pivo> findByPivaraId(Long pivaraId, Pageable pageRequest);
	
	@Query("SELECT p FROM Pivo p WHERE "
			+ "(:naziv IS NULL or p.naziv like :naziv ) AND "
			+ "(:minIbu IS NULL or p.ibu like :minIbu ) AND "
			+ "(:maxIbu IS NULL OR p.ibu <= :maxIbu) AND "
			+ "(:pivaraId IS NULL or p.pivara.id = :pivaraId ) "
			)
	
	Page<Pivo> pretraga(
			@Param("naziv") String naziv, 
			@Param("minIbu") Double minIbu, 
			@Param("maxIbu") Double maxIbu, 
			@Param("pivaraId") Long pivaraId,
			Pageable pageRequest);
	
	

}
