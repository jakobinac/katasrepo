package jwd.knjizara;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import jwd.knjizara.model.Pivara;
import jwd.knjizara.model.Pivo;
import jwd.knjizara.service.PivaraService;
import jwd.knjizara.service.PivoService;

@Component
public class TestData {
	@Autowired
	private PivoService pivoService;
	@Autowired
	private PivaraService pivaraService;

	@PostConstruct
	public void init() {
		
		
		Pivara p1 = new Pivara();
        Pivara p2 = new Pivara();
        Pivara p3 = new Pivara();
        
        p1.setNaziv("Apatinska pivara");
        p1.setPIB("100962933");
        p1.setDrzava("Srbija");
        
        p2.setNaziv("Pivara Celarevo");
        p2.setPIB("100962852");
        p2.setDrzava("Srbija");
        
        p3.setNaziv("BIP");
        p3.setPIB("100962111");
        p3.setDrzava("Srbija");
        
        pivaraService.save(p1);
        pivaraService.save(p2);
        pivaraService.save(p3);
        
        
        Pivo pivo1 = new Pivo("Jelen", "Svetlo", 4.5, 22.5, 10, p1);
        Pivo pivo2 = new Pivo("Tuborg", "Svetlo", 4.5, 22.5, 11, p2);
        Pivo pivo3 = new Pivo("BIP", "Tamno", 6.5, 23.5, 1, p3);
        Pivo pivo4 = new Pivo("Lav", "Svetlo", 5.0, 21.5, 20, p2);
        
        pivoService.save(pivo1);
        pivoService.save(pivo2);
        pivoService.save(pivo3);
        pivoService.save(pivo4);

	}
}