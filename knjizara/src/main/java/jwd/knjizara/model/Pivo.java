package jwd.knjizara.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="pivo")
public class Pivo {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(unique = true)
	private String naziv;
	@Column
	private String vrsta;
	@Column
	private Double procenatAlkohola;
	@Column
	private Double ibu;
	@Column(nullable=false)
	private Integer kolicinaNaStanju;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Pivara pivara;

	public Pivo() {
		super();
	}

	public Pivo(String naziv, String vrsta, Double procenatAlkohola, Double ibu, Integer kolicinaNaStanju,
			Pivara pivara) {
		super();
		this.naziv = naziv;
		this.vrsta = vrsta;
		this.procenatAlkohola = procenatAlkohola;
		this.ibu = ibu;
		this.kolicinaNaStanju = kolicinaNaStanju;
		this.pivara = pivara;
	}

	public Pivo(Long id, String naziv, String vrsta, Double procenatAlkohola, Double ibu, Integer kolicinaNaStanju,
			Pivara pivara) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.vrsta = vrsta;
		this.procenatAlkohola = procenatAlkohola;
		this.ibu = ibu;
		this.kolicinaNaStanju = kolicinaNaStanju;
		this.pivara = pivara;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public Double getProcenatAlkohola() {
		return procenatAlkohola;
	}

	public void setProcenatAlkohola(Double procenatAlkohola) {
		this.procenatAlkohola = procenatAlkohola;
	}

	public Double getIbu() {
		return ibu;
	}

	public void setIbu(Double ibu) {
		this.ibu = ibu;
	}

	public Integer getKolicinaNaStanju() {
		return kolicinaNaStanju;
	}

	public void setKolicinaNaStanju(Integer kolicinaNaStanju) {
		this.kolicinaNaStanju = kolicinaNaStanju;
	}

	public Pivara getPivara() {
		return pivara;
	}

	public void setPivara(Pivara pivara) {

		this.pivara = pivara;
		if(pivara!=null && !pivara.getPiva().contains(this)) {
			pivara.getPiva().add(this);
		}
	
	}
	
	
	
	


}
