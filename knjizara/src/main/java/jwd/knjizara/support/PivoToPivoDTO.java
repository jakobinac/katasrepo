package jwd.knjizara.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.knjizara.model.Pivo;
import jwd.knjizara.web.dto.PivoDTO;

@Component
public class PivoToPivoDTO implements Converter<Pivo, PivoDTO>	 {

	@Override
	public PivoDTO convert(Pivo s) {

		PivoDTO dto = new PivoDTO();
		
		dto.setId(s.getId());
		dto.setIbu(s.getIbu());
		dto.setKolicinaNaStanju(s.getKolicinaNaStanju());
		dto.setNaziv(s.getNaziv());
		dto.setProcenatAlkohola(s.getProcenatAlkohola());
		dto.setVrsta(s.getVrsta());
		dto.setPivaraid(s.getPivara().getId());
		dto.setPivaraNaziv(s.getPivara().getNaziv());
		
		
		return dto;

	}
	
	public List<PivoDTO> convert(List<Pivo> piva){
		List<PivoDTO> ret = new ArrayList<>();
		
		for(Pivo p : piva){
			ret.add(convert(p));
		}
		
		return ret;
	}

}
