package jwd.knjizara.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.knjizara.model.Pivo;
import jwd.knjizara.service.PivaraService;
import jwd.knjizara.service.PivoService;
import jwd.knjizara.web.dto.PivoDTO;

@Component
public class PivoDTOToPivo implements Converter<PivoDTO, Pivo> {
	
	@Autowired
	private PivaraService pivaraService;
	
	@Autowired
	private PivoService pivoService;

	@Override
	public Pivo convert(PivoDTO s) {
		Pivo pivo;
		if(s.getId()==null){
			pivo = new Pivo();
			pivo.setPivara(pivaraService.findOne(s.getPivaraid()));

		}else{
			pivo = pivoService.findOne(s.getPivaraid());
		}
		
		pivo.setNaziv(s.getNaziv());
		pivo.setVrsta(s.getVrsta());
		pivo.setProcenatAlkohola(s.getProcenatAlkohola());
		pivo.setIbu(s.getIbu());
		pivo.setKolicinaNaStanju(s.getKolicinaNaStanju());

		
		return pivo;
	}

}
