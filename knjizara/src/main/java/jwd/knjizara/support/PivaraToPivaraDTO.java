package jwd.knjizara.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import jwd.knjizara.model.Pivara;

import jwd.knjizara.web.dto.PivaraDTO;

@Component
public class PivaraToPivaraDTO implements Converter<Pivara, PivaraDTO> {

	@Override
	public PivaraDTO convert(Pivara s) {

		PivaraDTO pivaraDTO = new PivaraDTO();
		pivaraDTO.setId(s.getId());
		pivaraDTO.setNaziv(s.getNaziv());
		pivaraDTO.setPIB(s.getPIB());
		pivaraDTO.setDrzava(s.getDrzava());
		
		
		return pivaraDTO;
	
	}
	
	
	public List<PivaraDTO> convert(List<Pivara> pivare) {
		List<PivaraDTO> ret = new ArrayList<>();
		
		for(Pivara p: pivare){
			ret.add(convert(p));
		}
		
		return ret;
	}

}
