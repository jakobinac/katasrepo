package jwd.knjizara.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.knjizara.model.Pivara;
import jwd.knjizara.model.Pivo;
import jwd.knjizara.service.PivaraService;
import jwd.knjizara.service.PivoService;
import jwd.knjizara.support.PivaraToPivaraDTO;
import jwd.knjizara.support.PivoToPivoDTO;
import jwd.knjizara.web.dto.PivaraDTO;
import jwd.knjizara.web.dto.PivoDTO;

@RestController
@RequestMapping("/api/pivare")
public class ApiPivaraController {

	@Autowired
	private PivaraService pivaraService;

	@Autowired
	private PivaraToPivaraDTO toPivaraDTO;
	
	@Autowired
	private PivoService pivoService;
	
	@Autowired
	private PivoToPivoDTO toPivoDTO;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<PivaraDTO>> get(@RequestParam(defaultValue = "0") int page) {

		Page<Pivara> pivaraPage = pivaraService.findAll(page);

		if (page > pivaraPage.getTotalPages()) {
			return new ResponseEntity<List<PivaraDTO>>(HttpStatus.NOT_FOUND);
		}

		List<Pivara> pivare = pivaraPage.getContent();

		return new ResponseEntity<>(toPivaraDTO.convert(pivare), HttpStatus.OK);
	}


//	@RequestMapping(method = RequestMethod.GET)
//	public ResponseEntity<List<PivaraDTO>> get(@RequestParam(defaultValue = "0") int pageNum) {
//
//		Page<Pivara> pivare;
//
//		pivare = pivaraService.findAll(pageNum);
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("totalPages", Integer.toString(pivare.getTotalPages()));
//
//		return new ResponseEntity<>(toPivaraDTO.convert(pivare.getContent()), headers, HttpStatus.OK);
//	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<PivaraDTO> get(@PathVariable Long id) {

		Pivara pivara = pivaraService.findOne(id);

		if (pivara == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toPivaraDTO.convert(pivara), HttpStatus.OK);
	}

	@RequestMapping(value = "/{pivaraId}/piva")
	public ResponseEntity<List<PivoDTO>> pivoPivare (@PathVariable Long pivaraId,
			@RequestParam(defaultValue = "0") int pageNum) {
		Page<Pivo> piva = pivoService.findByPivaraId(pageNum, pivaraId);

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(piva.getTotalPages()));
		return new ResponseEntity<>(toPivoDTO.convert(piva.getContent()), headers, HttpStatus.OK);
	}

}
