package jwd.knjizara.web.dto;

import org.springframework.stereotype.Component;

@Component
public class PivaraDTO {
	
	private Long id;
    private String naziv;
	private String PIB;
	private String drzava;
	
	
	
	
	public PivaraDTO(String naziv, String pIB, String drzava) {
		super();
		this.naziv = naziv;
		this.PIB = pIB;
		this.drzava = drzava;
	}
	public PivaraDTO(Long id, String naziv, String pIB, String drzava) {
		super();
		this.id = id;
		this.naziv = naziv;
		PIB = pIB;
		this.drzava = drzava;
	}
	public PivaraDTO() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getPIB() {
		return PIB;
	}
	public void setPIB(String pIB) {
		PIB = pIB;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	
	

}
