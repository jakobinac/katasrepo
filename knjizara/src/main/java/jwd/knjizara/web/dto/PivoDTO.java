package jwd.knjizara.web.dto;

import org.springframework.stereotype.Component;

@Component
public class PivoDTO {
	
	private Long id;
	
	/*@NotBlank
	@Size(max=40)*/
	private String naziv;
	
	private String vrsta;
	
/*	@Min(value=0)
	@Max(value=100)*/
	private Double procenatAlkohola;
	
//	@Max(value = 100)
//	@Min(value = 0)
	private Double ibu;
	
//	@Min(value = 0)
//	@NotBlank
	private Integer kolicinaNaStanju;
	
	private Long pivaraid;
	
	private String pivaraNaziv;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public Double getProcenatAlkohola() {
		return procenatAlkohola;
	}

	public void setProcenatAlkohola(Double procenatAlkohola) {
		this.procenatAlkohola = procenatAlkohola;
	}

	public Double getIbu() {
		return ibu;
	}

	public void setIbu(Double ibu) {
		this.ibu = ibu;
	}

	public Integer getKolicinaNaStanju() {
		return kolicinaNaStanju;
	}

	public void setKolicinaNaStanju(Integer kolicinaNaStanju) {
		this.kolicinaNaStanju = kolicinaNaStanju;
	}

	public Long getPivaraid() {
		return pivaraid;
	}

	public void setPivaraid(Long pivaraid) {
		this.pivaraid = pivaraid;
	}

	public String getPivaraNaziv() {
		return pivaraNaziv;
	}

	public void setPivaraNaziv(String pivaraNaziv) {
		this.pivaraNaziv = pivaraNaziv;
	}

	
	
	
	

	
	
	
	
	
	
	

}
